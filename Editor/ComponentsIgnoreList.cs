/*************************************************************************
 *  Copyright © 2023-2030 Administrator. All rights reserved.
 *------------------------------------------------------------------------
 *  文件：ComponentsIgnoreList.cs
 *  作者：Administrator
 *  日期：2024/7/4 16:56:26
 *  公司：Fxb
 *  项目：com.fxb.copypastecomponents
 *  功能：Nothing
*************************************************************************/

using UnityEngine;
using System.Collections.Generic;

namespace CopyPasteComponents.Editor
{
    [System.Serializable]
    public class IgnoreItem
    { 
        public string assemblyName;
        public List<string> IgnoreTypes;
    }

    [CreateAssetMenu(fileName = "ComponentsIgnoreList", menuName = "ScriptableObjects/ComponentsIgnoreList", order = 1)]
	public class ComponentsIgnoreList : ScriptableObject
	{
        public List<IgnoreItem> IgnoreList = new List<IgnoreItem>();
    }
}

